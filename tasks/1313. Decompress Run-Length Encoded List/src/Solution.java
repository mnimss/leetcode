class Solution {

    public static void main(String[] args) {

    }

    public int[] decompressRLElist(int[] nums) {
        int finalLength = 0;
        for (int i = 0; i < nums.length; i += 2) {
            finalLength += nums[i];
        }

        int[] result = new int[finalLength];
        int l = 0;

        for (int i = 0, j = 1; i < nums.length; i += 2, j += 2) {
            Arrays.fill(result, l, l + nums[i], nums[j]);
            l += nums[i];
        }
        return result;
    }
}
