class Solution {

    public static void main(String[] args) {

    }

    public int subtractProductAndSum(int n) {
        int first = 1;
        int second = 0;

        while (n != 0) {
            first *= n % 10;
            second += n % 10;
            n /= 10;
        }
        return first - second;
    }
}
