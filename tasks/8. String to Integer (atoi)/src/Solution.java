class Solution {

    public static void main(String[] args) {

    }

    public int myAtoi(String str) {
        String trim = str.trim();
        boolean sign = true;
        if (trim.isEmpty()) {
            return 0;
        }
        int begin = 0;
        if (trim.charAt(0) < '0' || trim.charAt(0) > '9') {
            if (trim.length() > 1) {
                boolean secondSymbol = trim.charAt(1) < '0' || trim.charAt(1) > '9';
                if (trim.charAt(0) == '-') {
                    if (secondSymbol) {
                        return 0;
                    }
                    begin = 1;
                    sign = false;
                } else if (trim.charAt(0) == '+') {
                    if (secondSymbol) {
                        return 0;
                    }
                    begin = 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }
        int i = begin;
        char currentSymbol;
        while (i < trim.length()) {
            currentSymbol = trim.charAt(i);
            if (currentSymbol >= '0' && currentSymbol <= '9') {
                i++;
            } else {
                break;
            }
        }
        try {
            return (sign ? 1 : -1) * Integer.parseInt(trim.substring(begin, i));
        } catch (NumberFormatException e) {
            return sign ? Integer.MAX_VALUE : Integer.MIN_VALUE;
        }
    }
}
