import java.util.HashMap;
import java.util.Map;

class Solution {
    public static void main(String[] args) {

    }

    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int different = target - nums[i];
            if (map.containsKey(different)) {
                return new int[]{map.get(different), i};
            } else {
                map.put(nums[i], i);
            }
        }
        throw new IllegalArgumentException("Indexes not found");
    }
}