class Solution {

    public static void main(String[] args) {

    }

    public int findNumbers(int[] nums) {
        return (int) Arrays.stream(nums)
            .filter(a -> {
                return a >= 10 && a <= 99
                        || a >= 1000 && a <= 9999
                        || a >= 100000 && a <= 999999
                        || a >= 10000000 && a <= 99999999
                        || a >= 1000000000;
            })
            .count();
    }
}
