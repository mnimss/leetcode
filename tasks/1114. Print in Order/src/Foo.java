import java.util.concurrent.CountDownLatch;

class Foo {

    public static void main(String[] args) {

    }

    private CountDownLatch firstToSecond;
    private CountDownLatch secondToThird;

    public Foo() {
        firstToSecond = new CountDownLatch(1);
        secondToThird = new CountDownLatch(1);
    }

    public void first(Runnable printFirst) throws InterruptedException {

        // printFirst.run() outputs "first". Do not change or remove this line.
        printFirst.run();

        firstToSecond.countDown();
    }

    public void second(Runnable printSecond) throws InterruptedException {
        firstToSecond.await();

        // printSecond.run() outputs "second". Do not change or remove this line.
        printSecond.run();
        secondToThird.countDown();
    }

    public void third(Runnable printThird) throws InterruptedException {
        secondToThird.await();

        // printThird.run() outputs "third". Do not change or remove this line.
        printThird.run();
    }
}
