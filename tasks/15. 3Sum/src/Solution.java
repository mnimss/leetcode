class Solution {

    public static void main(String[] args) {

    }

    public List<List<Integer>> threeSum(int[] nums) {
        int target = 0;

        Map<Integer, Long> uniqueValues = Arrays
                    .stream(nums)
                    .boxed()
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        Set<List<Integer>> result = new HashSet<>();

        for (int i : uniqueValues.keySet()) {
            for (int j : uniqueValues.keySet()) {
                int different = target - i - j;
                if (uniqueValues.containsKey(different)) {
                    AtomicBoolean isOk = new AtomicBoolean(true);
                    Stream.of(i, j, different)
                            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                            .forEach((key, value) -> {
                                if (uniqueValues.get(key) < value) {
                                    isOk.set(false);
                                }
                            });
                    if (isOk.get()) {
                        List<Integer> list = new ArrayList<>(3);
                        list.add(i);
                        list.add(j);
                        list.add(different);

                        list.sort(Integer::compareTo);
                        result.add(list);
                    }
                }
            }
        }
        return new ArrayList<>(result);
    }
}
