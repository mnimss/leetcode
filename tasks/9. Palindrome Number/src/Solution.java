class Solution {

    public static void main(String[] args) {

    }

    public boolean isPalindrome(int x) {
        char[] string = String.valueOf(x).toCharArray();
        for (int i = 0, j = string.length - 1; i < j; i++, j--) {
            if (string[i] != string[j]) {
                return false;
            }
        }
        return true;
    }
}
