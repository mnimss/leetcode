class Solution {

    public static void main(String[] args) {

    }

    public String convert(String s, int numRows) {
        StringBuilder[] list = new StringBuilder[numRows];
        for (int i = 0; i < numRows; i++) {
            list[i] = new StringBuilder();
        }
        char[] string = s.toCharArray();
        boolean down = true;

        for (int i = 0, j = 0; j < string.length; j++) {
            list[i].append(string[j]);
            if (i + 1 == numRows) {
                down = false;
            } else if (i == 0) {
                down = true;
            }
            if (down) {
                i = ++i % numRows;
            } else {
                i = --i % numRows;
            }
        }

        return Arrays
                .stream(list)
                .reduce(StringBuilder::append)
                .map(String::new)
                .orElse("");
    }
}
