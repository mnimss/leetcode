class Solution {

    public static void main(String[] args) {

    }

    public int numJewelsInStones(String J, String S) {
        int result = 0;
        for (int j = 0; j < S.length(); j++) {
            for (int i = 0; i < J.length(); i++) {
                if (J.charAt(i) == S.charAt(j)) {
                    result++;
                    break;
                }
            }
        }
        return result;
    }
}
