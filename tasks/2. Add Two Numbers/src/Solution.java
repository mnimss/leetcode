class Solution {
    public static void main(String[] args) {

    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode result = new ListNode(0);
        ListNode current = result;
        int carry = 0;

        while (l1 != null && l2 != null) {
            int sum = l1.val + l2.val + carry;
            carry = sum / 10;
            current.val = sum % 10;
            if (l1.next != null && l2.next != null) {
                current.next = new ListNode(0);
                current = current.next;
            }
            l1 = l1.next;
            l2 = l2.next;
        }
        endAppend(l1 != null ? l1 : l2, current, carry);

        return result;
    }

    private void endAppend(ListNode listNode, ListNode current, int carry) {
        while (listNode != null) {
            int sum = listNode.val + carry;
            carry = sum / 10;
            current.next = new ListNode(sum % 10);
            current = current.next;
            listNode = listNode.next;
        }
        if (carry != 0) {
            current.next = new ListNode(carry);
        }
    }
}

class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}