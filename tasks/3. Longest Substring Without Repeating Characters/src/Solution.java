class Solution {
    public static void main(String[] args) {

    }

    public int lengthOfLongestSubstring(String s) {
            int ans = 0;
            char[] characters = s.toCharArray();
            Map<Character, Integer> map = new HashMap<>();
            for (int j = 0, i = 0; j < characters.length; j++) {
                if (map.containsKey(characters[j])) {
                    i = Math.max(map.get(characters[j]), i);
                }
                ans = Math.max(ans, j - i + 1);
                map.put(characters[j], j + 1);
            }
            return ans;
        }
}
