class Solution {
	public int[] shuffle(int[] nums, int n) {
        int[] result = new int[nums.length];
        int halfLength = nums.length / 2;
        int counter = 0;
        for (int i = 0, j = halfLength; i < halfLength; i++, j++) {
            result[counter++] = nums[i];
            result[counter++] = nums[j];
        }
        return result;
    }
}
