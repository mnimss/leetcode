class Solution {

    public static void main(String[] args) {

    }

    public String longestPalindrome(String s) {
        int maxLen = 0;
        String maxSubstring = "";
        char[] input = s.toCharArray();

        for (int i = 0; i < input.length; i++) {
            for (int j = input.length - 1; j >= i; j--) {
                if (maxLen < j + 1 - i) {
                    if (input[i] == input[j]) {
                        if (isPalindromic(i, j, input)) {
                            maxLen = j + 1 - i;
                            maxSubstring = s.substring(i, j + 1);
                        }
                    }
                }
            }
        }
        return maxSubstring;
    }

    private boolean isPalindromic(int begin, int end, char[] input) {
        int i;
        int j;

        for (i = begin, j = end; i < j; i++, j--) {
            if (input[i] != input[j]) {
                return false;
            }
        }
        return true;
    }
}
