class Solution {

    public static void main(String[] args) {

    }

    public ListNode removeNthFromEnd(ListNode head, int n) {
        List<ListNode> list = new ArrayList<>();
        ListNode current = head;

        while (current != null) {
            list.add(current);
            current = current.next;
        }
        int before = list.size() - n - 1;
        if (before < 0) {
            if (list.size() == 1) {
                return null;
            } else {
                return list.get(1);
            }
        } else {
            if (before + 2 < list.size()) {
                list.get(before).next = list.get(before + 2);
            } else {
                list.get(before).next = null;
            }
            return head;
        }
    }
}

class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}
